# cartas-myl

Este es el repositorio para la pagina web https://myl.0x00.cl
Este proyecto tiene como fin archivar las ilustrciones y datos de las cartas Mitos y Leyendas.

## Requerimientos para la ejecución

- JekyllRB 4.0+

### Ejecución

1. git clone https://gitlab.com/0x00cl/cartas-myl.git
2. cd cartas-myl
3. bundle install
4. bundle exec jekyll serve

## Estructura del repositorio

| Directorio | Comentario |
|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| _data | Este directorio contiene todos los datos de las cartas MyL en formato YAML, cada edicion tiene un archivo YAML el cual contiene los datos de todas las cartas. |
|  assets | Contiene todos los logos, imagenes y archivos CSS que se utilicen en el proyecto. |
| _includes | Contiene archivos html basico que son incluidos en el directorio _layout |
| _layout | Contiene la estructura de las distintas paginas, como indexacion de cartas y las cartas mismas. |
| ediciones | Contiene los archivos para la indexacion de las cartas de cada edicion, se define un tag correspondiente al nombre de la edicion. |

Las imagenes de las cartas son almacenadas en Google Photos actualmente.

## Como aportar con cartas

Son dos las formas en las que se puede aportar con cartas. 

* Creando un [issue](https://gitlab.com/0x00cl/cartas-myl/-/issues) en el cual se debe ingresar los datos de la carta y una imagen de alta resolución sin modificar de la ilustración. (Más información al momento de aportar)

* Creando un [merge request](https://gitlab.com/0x00cl/cartas-myl/-/merge_requests) agregando los datos de la carta al archivo YAML correspondiente e incluyendo una imagen de la ilustración de alta resolución sin modificar. 