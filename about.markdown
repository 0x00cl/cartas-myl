---
layout: default
title: Acerca de MyL
permalink: /acerca-de-myl/
---

Esta pagina web fue creada por [Tomas Gutierrez](https://0x00.cl) con el proposito de archivar las ilustraciones y la información de las cartas Mitos y Leyendas (MyL), un juego de cartas coleccionables chileno.

El repositorio de esta pagina web está en [cartas-myl](https://gitlab.com/0x00cl/cartas-myl). Está alojado en GitLab y se construye con JekyllRB.